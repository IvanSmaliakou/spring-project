REGISTRY_URL=gcr.io/spatial-cargo-285410/spring-project
KEYFILE=$(shell cat keyfile.json)

.PHONY: docker_build
docker_build:
	docker build -t gcr.io/spatial-cargo-285410/spring-project  .

.PHONY: docker_auth
docker_auth:
	cat keyfile.json | docker login -u _json_key --password-stdin https://gcr.io

.PHONY: docker_push
docker_push:
	docker push $(REGISTRY_URL)

.PHONY: docker_run
docker_run:
	docker run gcr.io/spatial-cargo-285410/spring-project