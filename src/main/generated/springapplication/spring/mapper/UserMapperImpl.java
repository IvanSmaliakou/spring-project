package springapplication.spring.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import springapplication.spring.entity.UserEntity;
import springapplication.spring.model.UserCreateRequest;
import springapplication.spring.model.UserResponse;
import springapplication.spring.model.UserUpdateRequest;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-02-13T16:51:19+0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.6 (Ubuntu)"
)
public class UserMapperImpl implements UserMapper {

    @Override
    public UserEntity toEntity(UserCreateRequest createRequest) {
        if ( createRequest == null ) {
            return null;
        }

        UserEntity userEntity = new UserEntity();

        userEntity.setEmail( createRequest.getEmail() );
        userEntity.setFirstName( createRequest.getFirstName() );
        userEntity.setLastName( createRequest.getLastName() );
        userEntity.setPassword( createRequest.getPassword() );
        userEntity.setPhone( createRequest.getPhone() );

        return userEntity;
    }

    @Override
    public UserEntity toEntity(UserUpdateRequest updateRequest) {
        if ( updateRequest == null ) {
            return null;
        }

        UserEntity userEntity = new UserEntity();

        userEntity.setFirstName( updateRequest.getFirstName() );
        userEntity.setLastName( updateRequest.getLastName() );
        userEntity.setPassword( updateRequest.getPassword() );
        userEntity.setPhone( updateRequest.getPhone() );

        return userEntity;
    }

    @Override
    public UserResponse toResponse(UserEntity entity) {
        if ( entity == null ) {
            return null;
        }

        UserResponse userResponse = new UserResponse();

        userResponse.setId( entity.getId() );
        userResponse.setEmail( entity.getEmail() );
        userResponse.setFirstName( entity.getFirstName() );
        userResponse.setLastName( entity.getLastName() );
        userResponse.setRole( entity.getRole() );
        userResponse.setPhone( entity.getPhone() );

        return userResponse;
    }

    @Override
    public List<UserResponse> toResponses(List<UserEntity> users) {
        if ( users == null ) {
            return null;
        }

        List<UserResponse> list = new ArrayList<UserResponse>( users.size() );
        for ( UserEntity userEntity : users ) {
            list.add( toResponse( userEntity ) );
        }

        return list;
    }
}
