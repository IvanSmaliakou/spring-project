package springapplication.spring.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import springapplication.spring.model.*;

import springapplication.spring.security.SecurityService;
import springapplication.spring.service.UserService;


import javax.validation.Valid;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.ResponseEntity.status;

@RestController
@AllArgsConstructor
@Api(value = "Auth service")
@RequestMapping("/api/v1")
public class AuthApi {

    private final SecurityService securityService;

    private final AuthenticationManager authenticationManager;

    private final UserService service;

    @PostMapping("/register")
    @ApiParam(value = "UserCreateRequest", required = true)
    @ApiOperation(value = "Register a user", response = UserResponse.class)
    public ResponseEntity<UserResponse> register(
            @ApiParam(value = "UserEntity", required = true)
            @RequestBody @Valid UserCreateRequest user) {
        UserResponse created = service.create(user);

        return status(CREATED).body(created);
    }

    @PostMapping("/login")
    @ApiParam(value = "UserLoginRequest", required = true)
    @ApiOperation(value = "Login", response = UserLoginResponse.class)
    public ResponseEntity<UserLoginResponse> login(@RequestBody @Valid UserLoginRequest loginRequest) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                loginRequest.getEmail(),
                loginRequest.getPassword()
        );
        authenticationManager.authenticate(token);

        UserLoginResponse loginResponse = securityService.createNewTokenPair(loginRequest);

        return status(ACCEPTED).body(loginResponse);
    }

    @PostMapping("/refresh")
    @ApiOperation(value = "Refresh", response = RefreshResponse.class)
    public ResponseEntity<RefreshResponse> refresh(@RequestBody @Valid RefreshRequest refresh) {
        RefreshResponse response = securityService.refresh(refresh.getRefresh());
        return status(OK).body(response);
    }
}