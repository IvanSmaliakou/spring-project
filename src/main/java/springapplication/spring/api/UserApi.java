package springapplication.spring.api;

import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springapplication.spring.model.UserResponse;
import springapplication.spring.model.UserUpdateRequest;
import springapplication.spring.service.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.noContent;

@RestController
@AllArgsConstructor
@Api(value = "User API")
@RequestMapping("/api/v1")
public class UserApi {

    private final UserService userService;

    @GetMapping("/users/{id}")
    @ApiOperation(
            authorizations = {@Authorization(value = "Authorization")},
            value = "Get a user", response = UserResponse.class)
    public @ResponseBody
    ResponseEntity<UserResponse> getUser(@PathVariable UUID id) {
        UserResponse user = userService.getUserByID(id);
        return ok().body(user);
    }

    @GetMapping("/users/params")
    @ApiOperation(
            authorizations = {@Authorization(value = "Authorization")},
            value = "Get user by first name and last name", response = UserResponse.class)
    public ResponseEntity<UserResponse> getByFirstAndLastName(
            @ApiParam(value = "first name and last name")
            @RequestParam(name = "first_name") String firstName,
            @RequestParam(name = "last_name") String lastName) {
        UserResponse user = userService.findByFirstAndLastName(firstName, lastName);
        return ok().body(user);
    }

    @GetMapping("/users")
    @ApiOperation(
            authorizations = {@Authorization(value = "Authorization")},
            value = "Get all users", response = List.class)
    public ResponseEntity<List<UserResponse>> getAll(@RequestParam(name = "page", defaultValue = "0") Integer pageNo,
                                                     @RequestParam(name = "size", defaultValue = "10") Integer pageSize,
                                                     @RequestParam(name = "sort_by", defaultValue = "email") String sortBy) {
        return ok().body(userService.getAll(pageNo, pageSize, sortBy));
    }

    @PutMapping("/users")
    @ApiOperation(
            authorizations = {@Authorization(value = "Authorization")},
            value = "Update a user", response = UserResponse.class)
    public ResponseEntity<UserResponse> updateUser(
            @RequestBody @Valid UserUpdateRequest user) {
        UserResponse updated = userService.update(user);
        return ok().body(updated);
    }

    @DeleteMapping("/users/{id}")
    @ApiOperation(
            authorizations = {@Authorization(value = "Authorization")},
            value = "Delete user by id")
    public ResponseEntity<Void> deleteUser(
            @PathVariable UUID id) {
        userService.delete(id);
        return noContent().build();
    }

}