package springapplication.spring.entity;

import lombok.*;
import springapplication.spring.entity.enums.Role;
import springapplication.spring.entity.enums.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

import static javax.persistence.EnumType.STRING;

@Data
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@Table(name = "users", schema = "public")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @NonNull
    @Column(name = "created_at", nullable = false, updatable = false)
    private Date createdAt;

    @NonNull
    @Column(name = "deleted_at")
    private Date deletedAt;

    @NonNull
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @NonNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NonNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @NonNull
    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "phone")
    private String phone;

    @NonNull
    @Enumerated(STRING)
    @Column(name = "status")
    private Status status;


    @Column(name = "updated_at")
    private Date updatedAt;

    @NonNull
    @Enumerated(STRING)
    @Column(name = "role", nullable = false)
    private Role role;

}
