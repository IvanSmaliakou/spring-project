package springapplication.spring.entity.enums;

public enum Status {
    ACTIVE,
    NOT_ACTIVE,
    DELETED
}
