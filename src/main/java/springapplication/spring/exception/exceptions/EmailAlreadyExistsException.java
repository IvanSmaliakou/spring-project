package springapplication.spring.exception.exceptions;

import lombok.*;

@Getter
public class EmailAlreadyExistsException extends RuntimeException {

    final String message;

    public EmailAlreadyExistsException(String message) {
        this.message = "Email already exists: " + message;
    }

}
