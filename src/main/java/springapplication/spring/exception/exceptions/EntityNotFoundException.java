package springapplication.spring.exception.exceptions;

import lombok.Getter;

@Getter
public class EntityNotFoundException extends RuntimeException {

    final String message;

    public EntityNotFoundException(String message) {
        this.message = "Bad request: " + message;
    }

}
