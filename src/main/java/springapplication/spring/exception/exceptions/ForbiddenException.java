package springapplication.spring.exception.exceptions;

import lombok.Getter;

@Getter
public class ForbiddenException extends RuntimeException{

    final String message;

    public ForbiddenException(String message) {
        this.message = "Access is forbidden: " + message;
    }

}
