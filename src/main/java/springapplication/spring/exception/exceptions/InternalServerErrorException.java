package springapplication.spring.exception.exceptions;

import lombok.Getter;

@Getter
public class InternalServerErrorException extends RuntimeException {

    final String message;

    public InternalServerErrorException(String message, String url) {
        this.message = "Internal server error: " + message;
    }

}
