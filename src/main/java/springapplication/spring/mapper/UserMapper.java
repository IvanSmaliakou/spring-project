package springapplication.spring.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import springapplication.spring.model.UserCreateRequest;
import springapplication.spring.model.UserResponse;
import springapplication.spring.model.UserUpdateRequest;
import springapplication.spring.entity.UserEntity;

import java.util.List;

@Mapper
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "role", ignore = true)
    @Mapping(target = "status", ignore = true)
    UserEntity toEntity(UserCreateRequest createRequest);

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "role", source = "role")
    @Mapping(target = "status", ignore = true)
    UserEntity toEntity(UserUpdateRequest updateRequest);

    UserResponse toResponse(UserEntity entity);

    List<UserResponse> toResponses(List<UserEntity> users);

}
