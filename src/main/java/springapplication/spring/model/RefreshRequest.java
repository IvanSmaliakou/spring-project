package springapplication.spring.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class RefreshRequest {

    @JsonProperty("refresh_token")
    @NotEmpty(message = "refresh cant be empty")
    private String refresh;

}
