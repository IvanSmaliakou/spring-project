package springapplication.spring.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NonNull;

@Data
public class RefreshResponse {

    @NonNull
    @JsonProperty("access_token")
    String accessToken;

    @NonNull
    @JsonProperty("refresh_token")
    String refreshToken;

}
