package springapplication.spring.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;


@Data
public class UserCreateRequest {

    @JsonProperty("email")
    @Email(message = "email cant be null")
    private String email;

    @JsonProperty("password")
    @NotEmpty(message = "password name cant be empty")
    private String password;

    @JsonProperty("first_name")
    @NotEmpty(message = "last name cant be empty")
    private String firstName;

    @JsonProperty("last_name")
    @NotEmpty(message = "last name cant be empty")
    private String lastName;

    @JsonProperty("phone")
    private String phone;

}
