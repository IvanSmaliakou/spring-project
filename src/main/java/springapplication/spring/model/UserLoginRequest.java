package springapplication.spring.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class UserLoginRequest {

    @JsonProperty("email")
    @Email(message = "email cant be null")
    private String email;

    @JsonProperty("password")
    @NotEmpty(message = "password name cant be empty")
    private String password;

}

