package springapplication.spring.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Data
public class UserLoginResponse {

    @NotEmpty
    @JsonProperty("id")
    UUID id;

    @JsonProperty("email")
    String email;

    @JsonProperty("access_token")
    String accessToken;

    @JsonProperty("refresh_token")
    String refreshToken;

}
