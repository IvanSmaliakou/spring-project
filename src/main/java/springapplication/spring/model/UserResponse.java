package springapplication.spring.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import springapplication.spring.entity.enums.Role;

import java.util.UUID;

@Data
@NoArgsConstructor
public class UserResponse {

    @NonNull
    @JsonProperty("id")
    private UUID id;

    @NonNull
    @JsonProperty("email")
    private String email;

    @NonNull
    @JsonProperty("first_name")
    private String firstName;

    @NonNull
    @JsonProperty("last_name")
    private String lastName;

    @NonNull
    @JsonProperty("role")
    private Role role;

    @JsonProperty("phone")
    private String phone;

}
