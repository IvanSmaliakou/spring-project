package springapplication.spring.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import springapplication.spring.entity.enums.Role;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Data
public class UserUpdateRequest {

    @JsonProperty("email")
    @NotEmpty
    String email;

    @JsonProperty("role")
    @NotNull(message = "role cant be empty")
    private Role role;

    @JsonProperty("password")
    @NotEmpty(message = "password cant be empty")
    private String password;

    @JsonProperty("first_name")
    @NotEmpty(message = "first name cant be empty")
    private String firstName;

    @JsonProperty("last_name")
    @NotEmpty(message = "last name cant be empty")
    private String lastName;

    @JsonProperty("phone")
    private String phone;

}
