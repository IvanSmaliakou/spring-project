package springapplication.spring.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import springapplication.spring.entity.UserEntity;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    @Query("select u from UserEntity u where u.firstName = :first_name and u.lastName=:last_name")
    Optional<UserEntity> findByFirstAndLastName(@Param("first_name") String firstName,
                                                @Param("last_name") String lastName);

    @Query(value = "select * from users where email=:email", nativeQuery = true)
    Optional<UserEntity> findByEmail(@Param("email") String email);

    @Query(value = "select * from users where deleted_at IS NULL", nativeQuery = true)
    Page<UserEntity> findAllNotDeleted(Pageable pageable);

}
