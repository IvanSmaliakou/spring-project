package springapplication.spring.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import springapplication.spring.entity.UserEntity;
import springapplication.spring.entity.enums.Role;
import springapplication.spring.entity.enums.Status;

import java.util.LinkedList;
import java.util.List;

import static java.util.stream.Collectors.toList;

class JwtUserFactory {

    static JwtUser create(UserEntity user) {
        List<Role> roles = new LinkedList<>();
        roles.add(user.getRole());

        return new JwtUser(
                user.getId(),
                user.getEmail(),
                user.getFirstName(),
                user.getLastName(),
                user.getPassword(),
                user.getStatus().equals(Status.ACTIVE),
                toGrantedAuthorities(roles));
    }

    private static List<GrantedAuthority> toGrantedAuthorities(List<Role> roles) {
        return roles
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.name()))
                .collect(toList());
    }

}
