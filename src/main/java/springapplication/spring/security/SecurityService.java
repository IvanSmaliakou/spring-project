package springapplication.spring.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import springapplication.spring.entity.UserEntity;
import springapplication.spring.model.RefreshResponse;
import springapplication.spring.model.UserLoginRequest;
import springapplication.spring.model.UserLoginResponse;
import springapplication.spring.repository.UserRepository;
import springapplication.spring.exception.exceptions.EntityNotFoundException;
import springapplication.spring.exception.exceptions.ForbiddenException;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.security.Key;
import java.util.*;

import static org.springframework.security.core.context.SecurityContextHolder.*;


@Service
@RequiredArgsConstructor
public class SecurityService implements Serializable {

    // Token is valid within 5 minutes.
    private static final long JWT_TOKEN_VALIDITY = 5 * 60 * 1000;

    private final Key secret = Keys.secretKeyFor(SignatureAlgorithm.HS512);

    @NonNull
    private final UserRepository repository;

    @NonNull
    private final JwtUserDetailsService service;

    public UserLoginResponse createNewTokenPair(UserLoginRequest loginRequest) throws ForbiddenException {
        String email = loginRequest.getEmail();

        UserEntity user = repository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("user not found."));

        Map<String, Object> tokenData = new HashMap<>();

        tokenData.put("role", user.getRole());
        tokenData.put("email", email);

        long expiresIn = new Date(System.currentTimeMillis()).getTime() + JWT_TOKEN_VALIDITY;

        JwtBuilder jwtBuilder = Jwts.builder();
        jwtBuilder.setIssuedAt(new Date(System.currentTimeMillis()));
        jwtBuilder.setId(user.getId().toString());
        jwtBuilder.setExpiration(new Date(expiresIn));
        jwtBuilder.addClaims(tokenData);

        String access = jwtBuilder.signWith(secret).compact();

        JwtBuilder refreshBuilder = Jwts.builder();
        refreshBuilder.setIssuedAt(new Date(System.currentTimeMillis()));
        refreshBuilder.setId(user.getId().toString());
        refreshBuilder.addClaims(tokenData);

        String refresh = refreshBuilder.signWith(secret).compact();

        UserLoginResponse loginResponse = new UserLoginResponse();

        loginResponse.setEmail(email);
        loginResponse.setId(user.getId());
        loginResponse.setAccessToken(access);
        loginResponse.setRefreshToken(refresh);

        return loginResponse;
    }

    public RefreshResponse refresh(String token) throws ForbiddenException {
        UserDetails userDetails = service.loadUserByUsername(this.getEmail(token));
        String email = userDetails.getUsername();
        String password = userDetails.getPassword();

        UserLoginRequest request = new UserLoginRequest();
        request.setEmail(email);
        request.setPassword(password);
        UserLoginResponse newTokenPair = createNewTokenPair(request);

        String accessToken = newTokenPair.getAccessToken();
        String refreshToken = newTokenPair.getRefreshToken();

        clearContext();
        Authentication newAuth = getAuth(accessToken);
        getContext().setAuthentication(newAuth);

        return new RefreshResponse(accessToken, refreshToken);
    }

    String resolveToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization");

        if (token != null && token.startsWith("Bearer")) {
            return token.substring(7);
        }
        return null;
    }

    boolean validateToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            if (claims.getBody().getExpiration().after(new Date())) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }

        return false;
    }

    Authentication getAuth(String token) {
        UserDetails userDetails = service.loadUserByUsername(this.getEmail(token));

        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public String getEmail(String token) {
        try {
            return Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody()
                    .get("email", String.class);
        } catch (Exception ex) {
            throw new ForbiddenException("wrong token provided");
        }
    }


}
