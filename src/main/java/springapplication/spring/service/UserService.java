package springapplication.spring.service;

import springapplication.spring.model.UserCreateRequest;
import springapplication.spring.model.UserResponse;
import springapplication.spring.model.UserUpdateRequest;

import java.util.List;
import java.util.UUID;

public interface UserService {

    UserResponse create(UserCreateRequest user);

    void delete(UUID id);

    UserResponse findByFirstAndLastName(String firstName, String lastName);

    UserResponse update(UserUpdateRequest user);

    UserResponse getUserByID(UUID id);

    UserResponse getByEmail(String  email);

    List<UserResponse> getAll(Integer pageNumber, Integer pageSize, String sortBy);

}
