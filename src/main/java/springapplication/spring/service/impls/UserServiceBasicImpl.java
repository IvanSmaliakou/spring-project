package springapplication.spring.service.impls;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import springapplication.spring.entity.UserEntity;
import springapplication.spring.entity.enums.Role;
import springapplication.spring.exception.exceptions.EmailAlreadyExistsException;
import springapplication.spring.exception.exceptions.EntityNotFoundException;
import springapplication.spring.exception.exceptions.ForbiddenException;
import springapplication.spring.model.UserCreateRequest;
import springapplication.spring.model.UserResponse;
import springapplication.spring.model.UserUpdateRequest;
import springapplication.spring.repository.UserRepository;
import springapplication.spring.service.UserService;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.data.domain.Sort.by;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import static springapplication.spring.entity.enums.Role.ROLE_USER;
import static springapplication.spring.entity.enums.Status.ACTIVE;
import static springapplication.spring.mapper.UserMapper.USER_MAPPER;


@Slf4j
@Service
@AllArgsConstructor
public class UserServiceBasicImpl implements UserService {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserResponse create(UserCreateRequest request) {
        String email = request.getEmail();
        if (repository.findByEmail(email).isPresent()) {
            throw new EmailAlreadyExistsException(email);
        }

        UserEntity entity = USER_MAPPER.toEntity(request);

        String plainPassword = entity.getPassword();
        entity.setPassword(passwordEncoder.encode(plainPassword));

        entity.setRole(ROLE_USER);
        entity.setStatus(ACTIVE);
        entity.setCreatedAt(new Date());

        UserEntity savedUser = repository.save(entity);

        log.info("CREATED" + savedUser);
        return USER_MAPPER.toResponse(savedUser);
    }

    @Override
    public void delete(UUID id) {
        UserEntity user = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("user not found."));
        if (!currentUserIsAdmin() && !idIsOfCurrentUser(id)) {
            throw new ForbiddenException("you do not have enough rights to access this data.");
        }
        user.setDeletedAt(new Date());
        repository.save(user);

        log.info("USER WITH ID" + id.toString() + " IS DELETED");
    }

    @Override
    public UserResponse findByFirstAndLastName(String firstName, String lastName) {
        UserEntity user = repository.findByFirstAndLastName(firstName, lastName)
                .orElseThrow(() -> new EntityNotFoundException("user not found."));

        return USER_MAPPER.toResponse(user);
    }

    @Override
    public UserResponse update(UserUpdateRequest user) {
        String email = user.getEmail();
        UserEntity dbUser = repository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("user not found."));
        if (!currentUserIsAdmin() && !idIsOfCurrentUser(dbUser.getId())) {
            throw new ForbiddenException("you do not have enough rights to access this data.");
        }
        String encoded = passwordEncoder.encode(user.getPassword());

        dbUser.setPassword(encoded);
        dbUser.setFirstName(user.getFirstName());
        dbUser.setLastName(user.getLastName());
        dbUser.setPhone(user.getPhone());
        dbUser.setRole(user.getRole());

        UserEntity updatedUser = repository.save(dbUser);
        log.info("USER" + updatedUser.toString() + " IS UPDATED");

        return USER_MAPPER.toResponse(updatedUser);
    }

    @Override
    public UserResponse getUserByID(UUID id) {
        UserEntity user = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("user not found."));
        if (!currentUserIsAdmin() && !idIsOfCurrentUser(id)) {
            throw new ForbiddenException("you do not have enough rights to access this data.");
        }

        return USER_MAPPER.toResponse(user);
    }

    @Override
    public UserResponse getByEmail(String email) {
        UserEntity user = repository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("user not found."));

        if (!currentUserIsAdmin() && !emailIsOfCurrentUser(email)) {
            throw new ForbiddenException("you do not have enough rights to access this data.");
        }
        return USER_MAPPER.toResponse(user);
    }

    @Override
    public List<UserResponse> getAll(Integer pageNumber, Integer pageSize, String sortBy) {
        Pageable paging = of(pageNumber, pageSize, by(sortBy).ascending());
        Page<UserEntity> pageWithUsers;
        if (currentUserIsAdmin()) {
            pageWithUsers = repository.findAll(paging);
        } else {
            pageWithUsers = repository.findAllNotDeleted(paging);
        }
        List<UserEntity> pageContent = pageWithUsers.getContent();
        return USER_MAPPER.toResponses(pageContent);
    }

    private boolean currentUserIsAdmin() {
        String email = getContext().getAuthentication().getName();
        UserEntity currentUser = repository.findByEmail(email)
                .orElseThrow(() -> new ForbiddenException("cannot get user from security context"));

        return currentUser.getRole().equals(Role.ROLE_ADMIN);
    }

    private boolean idIsOfCurrentUser(UUID id) {
        String email = getContext().getAuthentication().getName();
        UserEntity currentUser = repository.findByEmail(email)
                .orElseThrow(() -> new ForbiddenException("cannot get user from security context"));
        return currentUser.getId().equals(id);
    }

    private boolean emailIsOfCurrentUser(String email) {
        return getContext().getAuthentication().getName().equals(email);
    }

}