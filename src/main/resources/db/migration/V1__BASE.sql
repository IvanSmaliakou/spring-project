create table if not exists users
(
    id uuid not null constraint users_pkey primary key,
    created_at timestamp not null,
    deleted_at timestamp,
    email      varchar(255) not null,
    first_name varchar(255) not null,
    last_name  varchar(255) not null,
    password   varchar(255) not null,
    phone      varchar(255),
    role       varchar(255) not null,
    status     varchar(255),
    updated_at timestamp
);

alter table users
    owner to "postgres";

