package spingapplication.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import springapplication.spring.Application;
import springapplication.spring.model.*;
import springapplication.spring.repository.UserRepository;

import java.util.UUID;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.core.context.SecurityContextHolder.clearContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static springapplication.spring.entity.enums.Role.ROLE_ADMIN;

@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public abstract class AbstractTestClass {

    protected final String USERS_PREFIX = "/users";
    protected static final String BASE_URL = "/api/v1";
    protected static final String BEARER_PREFIX = "Bearer_";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @Before
    public void prepareDatabase() {
        userRepository.deleteAll();
    }

    // USER PREPARATION
    public UserResponse createUser(UserCreateRequest request) throws Exception {
        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/register")
                 .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andReturn();
        UserResponse createdUser = objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), UserResponse.class);
        return createdUser;
    }

    public UserResponse updateUser(UserUpdateRequest request, String token) throws Exception {
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + USERS_PREFIX)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + token)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), UserResponse.class);
    }

    public void deleteUser(UUID id, String token) throws Exception {
        mockMvc.perform(delete(BASE_URL + USERS_PREFIX + "/" + id)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + token))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    public UserLoginResponse getRefreshAndAccessToken(UserCreateRequest request) throws Exception {
        UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setEmail(request.getEmail());
        loginRequest.setPassword(request.getPassword());

        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/login")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequest)))
                .andExpect(status().isAccepted())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), UserLoginResponse.class);
    }

    public UserResponse createAdmin(UserCreateRequest request, UserUpdateRequest updateRequest) throws Exception {
        createUser(request);
        UserLoginResponse tokens = getRefreshAndAccessToken(request);
        updateRequest.setEmail(request.getEmail());
        updateRequest.setPassword(request.getPassword());
        updateRequest.setRole(ROLE_ADMIN);

        return updateUser(updateRequest, tokens.getAccessToken());
    }

    public UserResponse createUserNotInSession(UserCreateRequest request) throws Exception {
        UserResponse user = createUser(request);
        clearContext();
        return user;
    }

}
