package spingapplication.spring.integration;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.ResultActions;
import spingapplication.spring.AbstractTestClass;
import springapplication.spring.Application;
import springapplication.spring.model.RefreshRequest;
import springapplication.spring.model.UserCreateRequest;
import springapplication.spring.model.UserLoginRequest;
import springapplication.spring.model.UserLoginResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static spingapplication.spring.utils.Generator.generateLoginRequest;
import static spingapplication.spring.utils.Generator.generateUserCreateRequest;
import static springapplication.spring.entity.enums.Role.ROLE_USER;

@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class TestAuthApi extends AbstractTestClass {

    private final String WRONG_JWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
    @Test
    public void register_BadPayload() throws Exception {
        final UserCreateRequest request = generateUserCreateRequest();
        request.setEmail("123");

        // WHEN
        ResultActions action = this.mockMvc.perform(post(BASE_URL + "/register")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));

        // THEN
        action.andExpect(status().isBadRequest());
    }

    @Test
    public void register_happyPath() throws Exception {
        // GIVEN
        final UserCreateRequest request = generateUserCreateRequest();

        // WHEN
        ResultActions action = this.mockMvc.perform(post(BASE_URL + "/register")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));

        // THEN
        action.andExpect(status().isCreated());
        action.andExpect(content().string(Matchers.containsString(ROLE_USER.toString())));
        action.andExpect(content().string(Matchers.containsString(request.getEmail())));

    }

    @Test
    public void login_BadPayload() throws Exception {
        final UserLoginRequest request = generateLoginRequest();
        request.setEmail("123");

        // WHEN
        ResultActions action = this.mockMvc.perform(post(BASE_URL + "/login")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));

        // THEN
        action.andExpect(status().isBadRequest());
    }

    @Test
    public void login_WrongPassword() throws Exception {
        UserLoginRequest request = generateLoginRequest();

        // WHEN
        ResultActions action = this.mockMvc.perform(post(BASE_URL + "/login")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));

        // THEN
        action.andExpect(status().isForbidden());
    }

    @Test
    public void login_happyPath() throws Exception {
        UserCreateRequest createRequest = generateUserCreateRequest();
        UserLoginRequest request = generateLoginRequest();
        request.setEmail(createRequest.getEmail());
        request.setPassword(createRequest.getPassword());

        createUser(createRequest);

        // WHEN
        ResultActions action = this.mockMvc.perform(post(BASE_URL + "/login")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));

        // THEN
        action.andExpect(status().isAccepted());
        action.andExpect(content().string(Matchers.containsString(request.getEmail())));
    }

    @Test
    public void refresh_BadPayload() throws Exception {
        // GIVEN
        RefreshRequest request = new RefreshRequest();
        request.setRefresh(WRONG_JWT);
        // WHEN
        ResultActions action = this.mockMvc.perform(post(BASE_URL + "/refresh")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));

        // THEN
        action.andExpect(status().isForbidden());
    }

    @Test
    public void refresh_happyPath() throws Exception {
        // GIVEN
        UserCreateRequest request = generateUserCreateRequest();
        createUser(request);
        UserLoginResponse loginResponse = getRefreshAndAccessToken(request);

        RefreshRequest refreshRequest = new RefreshRequest();
        refreshRequest.setRefresh(loginResponse.getRefreshToken());

        // WHEN
        ResultActions action = this.mockMvc.perform(post(BASE_URL + "/refresh")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(refreshRequest)));

        // THEN
        action.andExpect(status().isOk());
    }

}
