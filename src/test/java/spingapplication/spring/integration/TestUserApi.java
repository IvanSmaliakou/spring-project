package spingapplication.spring.integration;

import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import spingapplication.spring.AbstractTestClass;
import spingapplication.spring.utils.Generator;
import springapplication.spring.model.UserCreateRequest;
import springapplication.spring.model.UserLoginResponse;
import springapplication.spring.model.UserResponse;
import springapplication.spring.model.UserUpdateRequest;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static spingapplication.spring.utils.Generator.generateUserCreateRequest;
import static spingapplication.spring.utils.Generator.generateUserUpdateRequest;

public class TestUserApi extends AbstractTestClass {

    protected final String USERS_PREFIX = "/users";
    protected final String WRONG_FIRST_NAME = "ohhhhhh";
    protected final String WRONG_LAST_NAME = "nooope";

    @Test
    public void getUser_WhenWrongId() throws Exception {
        // GIVEN
        UserCreateRequest request = generateUserCreateRequest();
        UserResponse user = createUser(request);
        user.setId(UUID.randomUUID());
        UserLoginResponse refreshAndAccessToken = getRefreshAndAccessToken(request);

        // WHEN
        ResultActions action = this.mockMvc.perform(get(BASE_URL + USERS_PREFIX + user.getId())
                .header(AUTHORIZATION, BEARER_PREFIX + refreshAndAccessToken.getAccessToken())
                .contentType(APPLICATION_JSON));

        // THEN
        action.andExpect(status().isNotFound());
    }

    @Test
    public void getUser_NotEnoughRights() throws Exception {
        // GIVEN
        UserCreateRequest notInSessionReq = generateUserCreateRequest();
        UserResponse userNotInSession = createUser(notInSessionReq);

        UserCreateRequest inSessionReq = generateUserCreateRequest();
        createUser(inSessionReq);
        UserLoginResponse sessionTokens = getRefreshAndAccessToken(inSessionReq);

        // WHEN
        ResultActions action = this.mockMvc.perform(get(BASE_URL + USERS_PREFIX + "/" + userNotInSession.getId())
                .header(AUTHORIZATION, BEARER_PREFIX + sessionTokens.getAccessToken())
                .contentType(APPLICATION_JSON));

        // THEN
        action.andExpect(status().isForbidden());
    }

    @Test
    public void getUser_HappyPathAdmin() throws Exception {
        // GIVEN
        UserCreateRequest notInSessionReq = generateUserCreateRequest();
        UserResponse userNotInSession = createUser(notInSessionReq);
        UserUpdateRequest updateRequest = generateUserUpdateRequest();

        UserCreateRequest inSessionReq = generateUserCreateRequest();
        createAdmin(inSessionReq, updateRequest);
        inSessionReq.setPassword(updateRequest.getPassword());
        UserLoginResponse sessionTokens = getRefreshAndAccessToken(inSessionReq);


        // WHEN
        ResultActions action = this.mockMvc.perform(get(BASE_URL + USERS_PREFIX + "/" + userNotInSession.getId())
                .header(AUTHORIZATION, BEARER_PREFIX + sessionTokens.getAccessToken())
                .contentType(APPLICATION_JSON));
        UserResponse response = objectMapper.readValue(action.andReturn().getResponse().getContentAsByteArray(), UserResponse.class);

        // THEN
        assertEquals(userNotInSession.getId(), response.getId());
        action.andExpect(status().isOk());
    }

    @Test
    public void getUser_HappyPathUser() throws Exception {
        // GIVEN
        UserCreateRequest request = generateUserCreateRequest();
        UserResponse user = createUser(request);
        UserLoginResponse refreshAndAccessToken = getRefreshAndAccessToken(request);

        // WHEN
        ResultActions action = this.mockMvc.perform(get(BASE_URL + USERS_PREFIX + "/" + user.getId())
                .header(AUTHORIZATION, BEARER_PREFIX + refreshAndAccessToken.getAccessToken())
                .contentType(APPLICATION_JSON));

        // THEN
        UserResponse response = objectMapper.readValue(action.andReturn().getResponse().getContentAsByteArray(), UserResponse.class);

        assertEquals(user.getId(), response.getId());
        action.andExpect(status().isOk());
    }

    @Test
    public void getByFirstAndLastName_WhenNameIsWrong() throws Exception {
        // GIVEN
        UserCreateRequest request = generateUserCreateRequest();
        UserResponse user = createUser(request);
        UserLoginResponse refreshAndAccessToken = getRefreshAndAccessToken(request);

        // WHEN
        ResultActions action = this.mockMvc.perform(get(BASE_URL + USERS_PREFIX + "/params")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + refreshAndAccessToken.getAccessToken())
                .param("first_name", WRONG_FIRST_NAME)
                .param("last_name", WRONG_LAST_NAME));

        // THEN
        action.andExpect(status().isNotFound());
    }

    @Test
    public void getByFirstAndLastName_HappyPathUser() throws Exception {
        // GIVEN
        UserCreateRequest request = generateUserCreateRequest();
        UserResponse user = createUser(request);
        UserLoginResponse refreshAndAccessToken = getRefreshAndAccessToken(request);

        // WHEN
        ResultActions action = this.mockMvc.perform(get(BASE_URL + USERS_PREFIX + "/params")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + refreshAndAccessToken.getAccessToken())
                .param("first_name", user.getFirstName())
                .param("last_name", user.getLastName()));

        // THEN
        UserResponse response = objectMapper.readValue(action.andReturn().getResponse().getContentAsByteArray(), UserResponse.class);

        assertEquals(user.getId(), response.getId());
        action.andExpect(status().isOk());
    }

    @Test
    public void getAll_HappyPathUser() throws Exception {
        // GIVEN
        UserCreateRequest request;
        UserLoginResponse refreshAndAccessToken = null;
        UUID[] ids = new UUID[10];
        String[] tokens = new String[10];

        for (int i = 0; i < 10; i++) {
            request = generateUserCreateRequest();
            UserResponse user = createUser(request);
            refreshAndAccessToken = getRefreshAndAccessToken(request);

            ids[i] = user.getId();
            tokens[i] = refreshAndAccessToken.getAccessToken();
        }
        deleteUser(ids[5], tokens[5]);
        deleteUser(ids[6], tokens[6]);
        deleteUser(ids[7], tokens[7]);

        // WHEN
        ResultActions action = this.mockMvc.perform(get(BASE_URL + USERS_PREFIX)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + refreshAndAccessToken.getAccessToken()));

        // THEN
        List response = objectMapper.readValue(action.andReturn().getResponse().getContentAsByteArray(), List.class);

        assertEquals(7, response.size());
        action.andExpect(status().isOk());
    }

    @Test
    public void getAll_HappyPathAdmin() throws Exception {
        // GIVEN

        UserCreateRequest request;
        UserLoginResponse refreshAndAccessToken = null;
        UUID[] ids = new UUID[10];
        String[] tokens = new String[10];

        for (int i = 0; i < 10; i++) {
            UserUpdateRequest updateRequest = Generator.generateUserUpdateRequest();
            request = generateUserCreateRequest();
            UserResponse user = createAdmin(request, updateRequest);
            request.setPassword(updateRequest.getPassword());
            refreshAndAccessToken = getRefreshAndAccessToken(request);

            ids[i] = user.getId();
            tokens[i] = refreshAndAccessToken.getAccessToken();
        }
        deleteUser(ids[5], tokens[5]);
        deleteUser(ids[6], tokens[6]);
        deleteUser(ids[7], tokens[7]);

        // WHEN
        ResultActions action = this.mockMvc.perform(get(BASE_URL + USERS_PREFIX)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + refreshAndAccessToken.getAccessToken()));

        // THEN
        List response = objectMapper.readValue(action.andReturn().getResponse().getContentAsByteArray(), List.class);

        assertEquals(10, response.size());
        action.andExpect(status().isOk());
    }

    @Test
    public void updateUser_WrongId() throws Exception {
        // GIVEN
        UserCreateRequest request = generateUserCreateRequest();
        createUser(request);
        UserUpdateRequest updateRequest = generateUserUpdateRequest();
        UserLoginResponse refreshAndAccessToken = getRefreshAndAccessToken(request);


        // WHEN
        ResultActions action = this.mockMvc.perform(put(BASE_URL + USERS_PREFIX)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + refreshAndAccessToken.getAccessToken())
                .content(objectMapper.writeValueAsString(updateRequest)));

        // THEN
        action.andExpect(status().isNotFound());
    }

    @Test
    public void updateUser_NotEnoughRights() throws Exception {
        // GIVEN
        UserCreateRequest notInSessionReq = generateUserCreateRequest();
        UserUpdateRequest updateRequest = generateUserUpdateRequest();
        updateRequest.setEmail(notInSessionReq.getEmail());
        createUser(notInSessionReq);

        UserCreateRequest inSessionReq = generateUserCreateRequest();
        inSessionReq.setEmail("email@example.com");
        createUser(inSessionReq);

        UserLoginResponse sessionTokens = getRefreshAndAccessToken(inSessionReq);


        // WHEN
        ResultActions action = this.mockMvc.perform(put(BASE_URL + USERS_PREFIX)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + sessionTokens.getAccessToken())
                .content(objectMapper.writeValueAsString(updateRequest)));

        // THEN
        action.andExpect(status().isForbidden());
    }

    @Test
    public void updateUser_HappyPathAdmin() throws Exception {
        // GIVEN
        UserCreateRequest notInSessionReq = generateUserCreateRequest();
        UserResponse userNotInSession = createUser(notInSessionReq);
        UserUpdateRequest userUpdateRequest = generateUserUpdateRequest();

        UserCreateRequest inSessionReq = generateUserCreateRequest();
        createAdmin(inSessionReq, userUpdateRequest);
        UserLoginResponse sessionTokens = getRefreshAndAccessToken(inSessionReq);


        // WHEN
        ResultActions action = this.mockMvc.perform(put(BASE_URL + USERS_PREFIX)
                .header(AUTHORIZATION, BEARER_PREFIX + sessionTokens.getAccessToken())
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userUpdateRequest)));

        // THEN
        action.andExpect(status().isOk());
    }

    @Test
    public void updateUser_HappyPathUser() throws Exception {
        // GIVEN
        UserCreateRequest request = generateUserCreateRequest();
        createUser(request);
        UserLoginResponse refreshAndAccessToken = getRefreshAndAccessToken(request);
        UserUpdateRequest userUpdateRequest = generateUserUpdateRequest();
        userUpdateRequest.setEmail(request.getEmail());

        // WHEN
        ResultActions action = this.mockMvc.perform(put(BASE_URL + USERS_PREFIX)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + refreshAndAccessToken.getAccessToken())
                .content(objectMapper.writeValueAsString(userUpdateRequest)));

        // THEN
        action.andExpect(status().isOk());
    }

    @Test
    public void deleteUser_WhenWrongId() throws Exception {
        // GIVEN
        UserCreateRequest request = generateUserCreateRequest();
        UserResponse user = createUser(request);
        user.setId(UUID.randomUUID());
        UserLoginResponse refreshAndAccessToken = getRefreshAndAccessToken(request);

        // WHEN
        ResultActions action = this.mockMvc.perform(delete(BASE_URL + USERS_PREFIX + "/" + user.getId())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + refreshAndAccessToken.getAccessToken()));

        // THEN
        action.andExpect(status().isNotFound());
    }

    @Test
    public void deleteUser_HappyPathAdmin() throws Exception {
        // GIVEN
        UserCreateRequest notInSessionReq = generateUserCreateRequest();
        UserResponse userNotInSession = createUser(notInSessionReq);
        UserUpdateRequest updateRequest = Generator.generateUserUpdateRequest();

        UserCreateRequest inSessionReq = generateUserCreateRequest();
        createAdmin(inSessionReq, updateRequest);
        UserLoginResponse sessionTokens = getRefreshAndAccessToken(inSessionReq);

        // WHEN
        ResultActions action = this.mockMvc.perform(delete(BASE_URL + USERS_PREFIX + "/" + userNotInSession.getId())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + sessionTokens.getAccessToken()));

        // THEN
        action.andExpect(status().isNoContent());
    }

    @Test
    public void deleteUser_HappyPathUser() throws Exception {
        // GIVEN
        UserCreateRequest request = generateUserCreateRequest();
        UserResponse user = createUser(request);
        UserLoginResponse refreshAndAccessToken = getRefreshAndAccessToken(request);

        // WHEN
        ResultActions action = this.mockMvc.perform(delete(BASE_URL + USERS_PREFIX + "/" + user.getId())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER_PREFIX + refreshAndAccessToken.getAccessToken()));

        // THEN
        action.andExpect(status().isNoContent());
    }

}
