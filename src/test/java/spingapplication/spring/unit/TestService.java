package spingapplication.spring.unit;

import com.github.javafaker.Faker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import springapplication.spring.Application;
import springapplication.spring.entity.UserEntity;
import springapplication.spring.exception.exceptions.EmailAlreadyExistsException;
import springapplication.spring.exception.exceptions.EntityNotFoundException;
import springapplication.spring.exception.exceptions.ForbiddenException;
import springapplication.spring.model.UserCreateRequest;
import springapplication.spring.model.UserResponse;
import springapplication.spring.model.UserUpdateRequest;
import springapplication.spring.repository.UserRepository;
import springapplication.spring.security.JwtUserDetailsService;
import springapplication.spring.service.UserService;
import springapplication.spring.service.impls.UserServiceBasicImpl;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.data.domain.Sort.by;
import static org.springframework.security.core.context.SecurityContextHolder.setContext;
import static spingapplication.spring.utils.Generator.generateUserCreateRequest;
import static spingapplication.spring.utils.Generator.generateUserUpdateRequest;
import static springapplication.spring.entity.enums.Role.ROLE_ADMIN;
import static springapplication.spring.entity.enums.Role.ROLE_USER;
import static springapplication.spring.mapper.UserMapper.USER_MAPPER;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = Application.class)
public class TestService {

    @Mock
    private UserRepository repository;

    JwtUserDetailsService tokenService = new JwtUserDetailsService(repository);

    @Mock
    private SecurityContextHolder securityContextHolder;

    @Test(expected = EmailAlreadyExistsException.class)
    public void createUserWhenEmailExists() {
        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest request = generateUserCreateRequest();
        UserEntity user = new UserEntity();
        user.setFirstName(request.getFirstName());

        when(repository.findByEmail(request.getEmail()))
                .thenReturn(of(user));

        service.create(request);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createUserRepositoryError() {
        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest request = generateUserCreateRequest();
        UserEntity entity = USER_MAPPER.toEntity(request);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);

        when(repository.findByEmail(request.getEmail()))
                .thenReturn(empty());
        when(repository.save(any()))
                .thenThrow(new IllegalArgumentException());
        UserResponse created = service.create(request);
        assertThat(created.getEmail())
                .isEqualTo(request.getEmail());
    }

    @Test
    public void createUserHappyPath() {
        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest request = generateUserCreateRequest();
        UserEntity entity = USER_MAPPER.toEntity(request);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);

        when(repository.findByEmail(request.getEmail()))
                .thenReturn(empty());
        when(repository.save(any()))
                .thenReturn(entity);
        UserResponse created = service.create(request);
        assertThat(created.getEmail())
                .isEqualTo(request.getEmail());
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteUser_NotFound() {
        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        UserEntity entity = USER_MAPPER.toEntity(createRequest);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);

        when(repository.findByEmail(createRequest.getEmail()))
                .thenReturn(empty());

        when(repository.save(any()))
                .thenReturn(entity);

        when(repository.findById(entity.getId()))
                .thenThrow(new EntityNotFoundException("testMsg"));

        service.create(createRequest);
        service.delete(entity.getId());
    }

    @Test(expected = ForbiddenException.class)
    public void deleteUser_IdIsNotOfCurrentUser() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        setContext(securityContext);

        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        UserEntity entity = USER_MAPPER.toEntity(createRequest);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);

        UserEntity fake = USER_MAPPER.toEntity(createRequest);
        fake.setId(randomUUID());

        when(repository.findByEmail(createRequest.getEmail()))
                .thenReturn(empty())
                .thenReturn(of(entity))
                .thenReturn(of(fake));
        when(repository.findById(entity.getId()))
                .thenReturn(of(entity));
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        when(securityContext.getAuthentication().getName())
                .thenReturn(entity.getEmail());

        setContext(securityContext);

        service.create(createRequest);
        service.delete(entity.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteUser_SaveError() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        setContext(securityContext);

        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        UserEntity entity = USER_MAPPER.toEntity(createRequest);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);

        when(repository.findByEmail(createRequest.getEmail()))
                .thenReturn(empty())
                .thenReturn(of(entity))
                .thenReturn(of(entity));

        when(repository.findById(entity.getId()))
                .thenReturn(of(entity));

        when(repository.save(any()))
                .thenReturn(entity)
                .thenThrow(new IllegalArgumentException());

        when(securityContext.getAuthentication())
                .thenReturn(authentication);

        when(securityContext.getAuthentication().getName())
                .thenReturn(entity.getEmail());

        setContext(securityContext);

        service.create(createRequest);
        service.delete(entity.getId());
    }

    @Test
    public void deleteUser_HappyPath() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        setContext(securityContext);

        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        UserEntity entity = USER_MAPPER.toEntity(createRequest);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);

        when(repository.findByEmail(createRequest.getEmail()))
                .thenReturn(empty())
                .thenReturn(of(entity))
                .thenReturn(of(entity));

        when(repository.findById(entity.getId()))
                .thenReturn(of(entity));

        when(repository.save(any()))
                .thenReturn(entity)
                .thenReturn(null);

        when(securityContext.getAuthentication())
                .thenReturn(authentication);

        when(securityContext.getAuthentication().getName())
                .thenReturn(entity.getEmail());

        setContext(securityContext);

        service.create(createRequest);
        service.delete(entity.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void findByFirstAndLastName_NotFound() {
        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        String firstName = createRequest.getFirstName();
        String lastName = createRequest.getLastName();

        when(repository.findByFirstAndLastName(firstName, lastName))
                .thenThrow(new EntityNotFoundException("testMsg"));

        service.create(createRequest);
        service.findByFirstAndLastName(firstName, lastName);
    }

    @Test
    public void findByFirstAndLastName_HappyPath() {
        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        String firstName = createRequest.getFirstName();
        String lastName = createRequest.getLastName();
        UserEntity entity = USER_MAPPER.toEntity(createRequest);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);

        when(repository.findByFirstAndLastName(firstName, lastName))
                .thenReturn(of(entity));

        service.create(createRequest);
        UserResponse resp = service.findByFirstAndLastName(firstName, lastName);

        assertThat(resp).isEqualTo(USER_MAPPER.toResponse(entity));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByEmail_NotFound() {
        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        String email = createRequest.getEmail();

        when(repository.findByEmail(email))
                .thenThrow(new EntityNotFoundException("testMsg"));

        service.create(createRequest);
        service.getByEmail(email);
    }

    @Test(expected = ForbiddenException.class)
    public void getByEmail_EmailIsNotOfCurrentUser() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        setContext(securityContext);

        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        UserEntity entity = USER_MAPPER.toEntity(createRequest);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);

        UserEntity fake = USER_MAPPER.toEntity(createRequest);
        fake.setId(randomUUID());
        fake.setRole(ROLE_USER);

        String email = entity.getEmail();

        when(repository.findByEmail(createRequest.getEmail()))
                .thenReturn(empty())
                .thenReturn(of(entity))
                .thenReturn(of(fake));
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        when(securityContext.getAuthentication().getName())
                .thenReturn(entity.getEmail())
                .thenReturn("fake@email.com");

        setContext(securityContext);

        service.create(createRequest);
        service.getByEmail(email);
    }

    @Test
    public void getByEmail_HappyPath() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        setContext(securityContext);

        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        UserEntity entity = USER_MAPPER.toEntity(createRequest);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);
        String email = entity.getEmail();

        when(repository.findByEmail(createRequest.getEmail()))
                .thenReturn(empty())
                .thenReturn(of(entity))
                .thenReturn(of(entity));
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        when(securityContext.getAuthentication().getName())
                .thenReturn(email);

        setContext(securityContext);

        service.create(createRequest);
        UserResponse response = service.getByEmail(email);

        assertThat(response).isEqualTo(USER_MAPPER.toResponse(entity));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserById_NotFound() {
        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        UserEntity userEntity = USER_MAPPER.toEntity(createRequest);
        userEntity.setId(randomUUID());
        UUID id = userEntity.getId();

        when(repository.findById(id))
                .thenThrow(new EntityNotFoundException("testMsg"));

        service.create(createRequest);
        service.getUserByID(id);
    }

    @Test(expected = ForbiddenException.class)
    public void getById_IdIsNotOfCurrentUser() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        setContext(securityContext);

        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        UserEntity entity = USER_MAPPER.toEntity(createRequest);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);

        UserEntity fake = USER_MAPPER.toEntity(createRequest);
        fake.setId(randomUUID());
        fake.setRole(ROLE_USER);

        UUID id = entity.getId();

        when(repository.findByEmail(createRequest.getEmail()))
                .thenReturn(empty())
                .thenReturn(of(entity))
                .thenReturn(of(fake));
        when(repository.findById(entity.getId()))
                .thenReturn(of(entity));
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        when(securityContext.getAuthentication().getName())
                .thenReturn(entity.getEmail());

        setContext(securityContext);

        service.create(createRequest);
        service.getUserByID(id);
    }

    @Test
    public void getById_HappyPath() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        setContext(securityContext);

        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserCreateRequest createRequest = generateUserCreateRequest();
        UserEntity entity = USER_MAPPER.toEntity(createRequest);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);
        UUID id = entity.getId();

        when(repository.findByEmail(createRequest.getEmail()))
                .thenReturn(empty())
                .thenReturn(of(entity));
        when(repository.findById(entity.getId()))
                .thenReturn(of(entity));
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        when(securityContext.getAuthentication().getName())
                .thenReturn(entity.getEmail());

        setContext(securityContext);

        service.create(createRequest);
        UserResponse response = service.getUserByID(id);

        assertThat(response).isEqualTo(USER_MAPPER.toResponse(entity));
    }

    @Test
    public void findAll_HappyPathAdmin() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        setContext(securityContext);

        UserCreateRequest userCreateRequest = generateUserCreateRequest();
        UserEntity user = USER_MAPPER.toEntity(userCreateRequest);
        user.setId(randomUUID());
        user.setRole(ROLE_ADMIN);

        List<UserEntity> users = new LinkedList<>();
        Pageable paging = PageRequest.of(0, 10, by("email").ascending());

        for (int i = 0; i < 10; i++) {
            UserCreateRequest createRequest = generateUserCreateRequest();
            UserEntity entity = USER_MAPPER.toEntity(createRequest);
            entity.setId(randomUUID());
            entity.setRole(ROLE_USER);
            users.add(entity);
        }

        Page<UserEntity> pagedUsers = new PageImpl<>(users, paging, users.size());
        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());

        when(repository.findAll(paging))
                .thenReturn(pagedUsers);
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        when(securityContext.getAuthentication().getName())
                .thenReturn(user.getEmail());
        when(repository.findByEmail(user.getEmail()))
                .thenReturn(of(user));

        List<UserResponse> results = service.getAll(0, 10, "email");

        assertThat(results.size()).isEqualTo(10);
    }

    @Test
    public void findAll_HappyPathUser() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        setContext(securityContext);

        UserCreateRequest userCreateRequest = generateUserCreateRequest();
        UserEntity user = USER_MAPPER.toEntity(userCreateRequest);
        user.setId(randomUUID());
        user.setRole(ROLE_USER);

        List<UserEntity> users = new LinkedList<>();
        Pageable paging = PageRequest.of(0, 10, by("email").ascending());

        for (int i = 0; i < 10; i++) {
            UserCreateRequest createRequest = generateUserCreateRequest();
            UserEntity entity = USER_MAPPER.toEntity(createRequest);
            entity.setId(randomUUID());
            entity.setRole(ROLE_USER);
            users.add(entity);
        }

        Page<UserEntity> pagedUsers = new PageImpl<>(users, paging, users.size());
        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());

        when(repository.findAllNotDeleted(paging))
                .thenReturn(pagedUsers);
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        when(securityContext.getAuthentication().getName())
                .thenReturn(user.getEmail());
        when(repository.findByEmail(user.getEmail()))
                .thenReturn(of(user));

        List<UserResponse> results = service.getAll(0, 10, "email");

        assertThat(results.size()).isEqualTo(10);
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_NotFound() {
        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserUpdateRequest request = generateUserUpdateRequest();
        String email = request.getEmail();

        when(repository.findByEmail(email))
                .thenReturn(empty());

        service.update(request);
    }

    @Test(expected = ForbiddenException.class)
    public void update_idIsNotOfCurrentUser() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        setContext(securityContext);

        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserUpdateRequest request = generateUserUpdateRequest();
        UserEntity entity = USER_MAPPER.toEntity(request);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);

        UserEntity fake = USER_MAPPER.toEntity(request);
        fake.setId(randomUUID());
        fake.setRole(ROLE_USER);

        when(repository.findByEmail(any()))
                .thenReturn(of(entity))
                .thenReturn(of(fake));
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        when(securityContext.getAuthentication().getName())
                .thenReturn(entity.getEmail());

        setContext(securityContext);

        service.update(request);
    }

    @Test
    public void update_HappyPath() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        setContext(securityContext);

        UserService service = new UserServiceBasicImpl(repository, new BCryptPasswordEncoder());
        UserUpdateRequest request = generateUserUpdateRequest();
        UserEntity entity = USER_MAPPER.toEntity(request);
        entity.setId(randomUUID());
        entity.setRole(ROLE_USER);

        when(repository.findByEmail(any()))
                .thenReturn(of(entity));
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        when(securityContext.getAuthentication().getName())
                .thenReturn(entity.getEmail());
        when(repository.save(any()))
                .thenReturn(entity);

        setContext(securityContext);
        UserResponse response = service.update(request);

        assertThat(response).isEqualTo(USER_MAPPER.toResponse(entity));
    }
}