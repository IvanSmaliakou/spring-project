package spingapplication.spring.utils;

import com.github.javafaker.Faker;
import springapplication.spring.model.UserCreateRequest;
import springapplication.spring.model.UserLoginRequest;
import springapplication.spring.model.UserUpdateRequest;

import static springapplication.spring.entity.enums.Role.ROLE_USER;

public class Generator {

    public static UserCreateRequest generateUserCreateRequest() {
        Faker faker = new Faker();
        UserCreateRequest request = new UserCreateRequest();
        request.setEmail(faker.internet().emailAddress());
        request.setPassword(faker.internet().password());
        request.setFirstName(faker.name().firstName());
        request.setLastName(faker.name().lastName());
        request.setPhone(faker.phoneNumber().cellPhone());
        return request;
    }

    public static UserUpdateRequest generateUserUpdateRequest() {
        Faker faker = new Faker();
        UserUpdateRequest request = new UserUpdateRequest();
        request.setEmail(faker.internet().emailAddress());
        request.setRole(ROLE_USER);
        request.setPassword(faker.internet().password());
        request.setFirstName(faker.name().firstName());
        request.setLastName(faker.name().lastName());
        request.setPhone(faker.phoneNumber().cellPhone());
        return request;
    }

    public static UserLoginRequest generateLoginRequest() {
        Faker faker = new Faker();
        UserLoginRequest request = new UserLoginRequest();
        request.setEmail(faker.internet().emailAddress());
        request.setPassword(faker.internet().password());
        return request;
    }

}
